---
title: Friendly Asked Questions
---
<br>

<div class="accordion mx-2" id="accordionExample">

  <div class="accordion-item">
    <h2 class="accordion-header" id="heading1">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
        Was ist Lützerath?
      </button>
    </h2>
    <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1" data-bs-parent="#accordionExample">
      <div class="accordion-body">
<p>Lützerath ist das letzte Dorf, das im Rheinischen Revier für den Kohleabbau abgebaggert werden soll. Die Kante des Braunkohletagebaus Garzweiler ist keine 200 Meter mehr von den Häusern Lützeraths entfernt. Wenn es nach den Plänen des Kohlekonzerns RWE geht, soll Lützerath diesen Herbst geräumt und abgerissen werden. Lützerath zu erhalten und die Kohle unter dem Dorf im Boden zu lassen, ist eine der effektivsten Klimaschutzmaßnahmen, die wir derzeit in Deutschland treffen können.</p>

<p>Lützerath ist in den letzten Jahren zu einem Kristallisationsort der Klimagerechtigkeitsbewegung geworden. Hunderte Klimaaktive haben Lützerath wiederbelebt, dort Baumhäuser und Holzhütten gebaut und Lützerath zu ihrem zu Hause gemacht. Immer wieder kamen mehrere Tausend Menschen zu Demonstrationen, Festivals und Konferenzen in dem Dorf zusammen. Eigentlich hätte RWE es längst zerstören wollen, doch gesellschaftlicher Druck und Gerichtsverfahren haben dies bisher verhindert – jetzt wollen wir das Dorf dauerhaft retten.</p>

<p>Am 01. September 2022 gehen die Gebäude und Flächen des Landwirts Eckhardt Heukamp an RWE. Es ist davon auszugehen, dass RWE diesen Herbst Lützerath räumen und abreißen lassen will. Ähnlich wie am Hambacher Forst ist dies nur mit einem politisch riskanten Großeinsatz der Polizei durchführbar. X-Tausende Menschen werden auf der Straße, auf Bäumen und in Häusern vor Ort für den Schutz des Klimas und den Erhalt des Dorfes protestieren und sich der Zerstörung in den Weg stellen. </p>
      </div>
    </div>
  </div>


  <div class="accordion-item">
    <h2 class="accordion-header" id="heading2">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
        Warum muss Lützerath erhalten bleiben?
      </button>
    </h2>
    <div id="collapse2" class="accordion-collapse collapse" aria-labelledby="heading2" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>Unter Lützerath liegt eine besonders dicke Schicht Braunkohle. Sie zu verbrennen schadet unserem Weltklima massiv. </p>
        <blockquote>
          <div class="row mb-3">
          <img>
            <div class="col-1 text-end"><img src="https://nitter.fdn.fr/pic/profile_images%2F1479884089582891010%2Fhv0SVCEl_bigger.jpg" style="max-width: 100%;max-height: 3em; border-radius:1.5em;border: 1px solid #ddd" /></div>
            <div class="col-9"><b>Alle Dörfer Bleiben</b><br><a href="https://twitter.com/AlleDoerfer" target="_blank">@AlleDoerfer</a></div>
            <div class="col-1 text-end fs-2"><a href="https://nitter.fdn.fr/AlleDoerfer/status/1535550955420372992" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
          </div>
          
          <p style="font-size:0.9em;font-family:sans-serif">Aktueller Planungsstand Tagebau Garzweiler:<br>
          🟤= momentaner Baggerfortschritt<br>
          ⚫️= durch Leitentscheidung 2016 gerettet<br>
          🟢= durch NRW-Sondierungspapier gerettet<br>
          ⚠️ Alles andere wollen <a href="https://twitter.com/CDUNRW_de" target="_blank">@CDUNRW_de</a> & <a href="https://twitter.com/gruenenrw" target="_blank">@gruenenrw</a> noch abbaggern lassen [Je roter die Flächen sind, desto mehr Kohle].</p>
          <img class="img-fluid" alt="Karte mit aktuellem Planungsstand" src="/user/pages/05.faq/karte.jpg" />
        </blockquote>
        <p>RWE plant, noch über 600 Millionen Tonnen Kohle am Tagebau Garzweiler zu fördern. Da eine Tonne Braunkohle bei ihrer Verbrennung circa eine Tonne CO2 freisetzt, entspricht die Klimaschädlichkeit dieser RWE-Pläne in etwa denen von 150 Millionen Flügen von Frankfurt nach New York. Diese Pläne sind nicht mit dem Pariser Klimaabkommen vereinbar. <a href="https://www.diw.de/documents/publikationen/73/diw_01.c.819609.de/diwkompakt_2021-169.pdf" target=_blank">Eine Studie</a> des Deutschen Instituts für Wirtschaftsforschung (DIW) zeigt, dass aus dem Tagebau Garzweiler ab 01.01.2021 lediglich 70 Millionen Tonnen Kohle gefördert werden dürften, um das 1,5°-Limit einzuhalten. Zwar tragen auch diese verringerten Mengen zur Verschärfung der Klimakrise bei, doch die Studie zeigt eindeutig: Die Rote Linie für das Klima verläuft vor Lützerath!
        </p>
      </div>
    </div>
  </div>



  <div class="accordion-item">
    <h2 class="accordion-header" id="heading11">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
        Ist Lützerath durch den Mehrbedarf an Kohle wegen des Ukraine-Kriegs nicht eh verloren?
      </button>
    </h2>
    <div id="collapse11" class="accordion-collapse collapse" aria-labelledby="heading11" data-bs-parent="#accordionExample">
      <div class="accordion-body">
                <p> Nein. Durch das Ersatzkraftwerkebereitstellungsgesetz (EKBG) werden im Rheinischen Braunkohlerevier drei Kraftwerksblöcke (3x ca. 300 MW) in die sogenannte Versorgungsreserve aufgenommen. Die Kraftwerke laufen jedoch nicht automatisch in voller Auslastung, sondern gehen erst dann ans Netz, wenn die Lage das erfordert. Zudem  ist die Anwendung des Gesetzes auf den Zeitraum Juli 2022 bis März 2024 beschränkt. Die (extrem unwahrscheinliche) volle Auslastung der drei Blöcke in dieser Zeit würde maximal 14 Mio t Kohle benötigen – das entspricht eher geringen 0,27 km² in der Fläche. Diese Kohlemengen müssten aus den Tagebauen Hambach und Garzweiler gewonnen werden, da die reaktivierten Kraftwerke ausschließlich an diese beiden Tagebaue angeschlossen sind. Zum Vergleich: Allein im Tagebau Garzweiler fördert RWE jährlich zwischen 20 und 40 Mio t Kohle. Im aktuellen Tagebaubereich sind bereits ca. 90 Mio t Kohle zugänglich. Der kurzfristig entstehende (maximale) Mehrbedarf kann daher gedeckt werden, ohne dass der Tagebau voran getrieben werden müsste. </p>
        <p> Mittelfristig muss die Kohle ohnehin im Boden bleiben, denn aus Klima-Perspektive darf all diese Kohle nicht verbrannt werden. Das zeigt eindrücklich diese Studie des Deutschen Instituts für Wirtschaftsforschung. Deshalb ist auch noch in der Diskussion, ob die Menge Kohle, die jetzt kurzfristig zusätzlich verbrannt wird, nicht am Ende des Tagebaubetriebs in der Erde gelassen werden muss. Aus all dem ergibt sich, dass weiterhin gilt: Lützerath kann und muss bleiben!hat auch der Bundestag zum Gesetz kommentiert: “Insofern im Zuge der Anwendung des EKGB es zu einer Verschlechterung der Klimabilanz kommt, gilt es entsprechende negative Folgewirkungen an anderer Stelle aufzufangen, etwa durch die Vermeidung weiterer Abbauten von Braunkohletagebauflächen.” </p>
        <p> Im Begleittext zum Gesetz haben die Abgeordneten außerdem festgehalten: „Der Deutsche Bundestag befürwortet zudem den Erhalt des Dorfes Lützerath am Tagebau Garzweiler und den Verzicht auf die Nutzung der Braunkohle unter dem Dorf.“</p>
        <p> RWE hat übrigens bereits heute die oberste Sohle des Tagebaus Garzweiler II so weit nach vorne getrieben, dass die förderbare Kohlemenge ausreichen würde, um ihre Kraftwerke in den nächsten zwei bis drei Jahren zu versorgen und nebenbei die 1,5-Grad-Grenze zu verfeuern. Der Tagebau muss also, auch wenn kurzfristig mehr Kohle verbrannt wird, gar nicht mehr vergrößert werden. Wenn wir unter 1,5 Grad bleiben wollen, darf er auch nicht mehr erweitert werden.</p>
        <p> Aus all dem ergibt sich: Lützerath kann und muss bleiben!</p>
      </div>
    </div>
  </div>



  <div class="accordion-item">
    <h2 class="accordion-header" id="heading12">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
        Ist es nicht besser, heimische Braunkohle zu verbrennen als Steinkohle, die unter noch schlimmeren Bedingungen in Kolumbien abgebaut wird?
      </button>
    </h2>
    <div id="collapse12" class="accordion-collapse collapse" aria-labelledby="heading12" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>Die Abbaubedingungen in Kolumbien sind tatsächlich katastrophal, die Ausweitung der Minen wird dort oft mit Mord, Vertreibung und Folter durchgesetzt. Wir fühlen uns eng verbunden mit den betroffenen Gemeinden und so schmerzt es uns besonders, dass durch das EKBG in Deutschland wieder mehr Steinkohle verbrannt werden soll. Es ist deshalb am Besten, weniger Energie für unsinnige Großprojekte, Aufrüstung, industrielle Landwirtschaft und ineffiziente Mobilität zu verschwenden – statt einfach nur russisches Gas durch Kohle zu ersetzen. </p>
      </div>
    </div>
  </div>




  <div class="accordion-item">
    <h2 class="accordion-header" id="heading13">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
        Ist Atomkraft nicht eine bessere Alternative als Kohle?
      </button>
    </h2>
    <div id="collapse13" class="accordion-collapse collapse" aria-labelledby="heading13" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>Nein. Gaskraftwerke haben vor allem den “Vorteil”, dass sie flexibel hoch und runtergefahren werden können, unter anderem auch um Spitzenlasten auszugleichen. Braunkohlekraftwerke sind bereits wesentlich unflexibler als Gaskraftwerke und sind daher nur bedingt eine Alternative. Atomkraftwerke hingegen sind nicht flexibel regulierbar und eignen sich folglich rein technisch nicht, um ausfallende Gaskraftwerke zu ersetzen. Zudem ist Atomkraft eine teure und extrem gefährliche Technologie, deren strahlendes Müllproblem weiterhin ungelöst ist. Das verschärft sich mit der Klimakatastrophe: in den Dürren und Hitzewellen, die wir erleben, ist es schwerer die Brennstäbe zu kühlen. Zudem ist ein Weiterbetrieb der Atomkraftwerke in Deutschland technisch nicht möglich: es wurde kein Personal mehr ausgebildet und keine Brennstäbe mehr bestellt. Deshalb befürworten nicht mal die Atomkonzerne den Wiedereinstieg in die Kernkraft. </p>
      </div>
    </div>
  </div>


<div class="accordion-item">
    <h2 class="accordion-header" id="heading3">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
        Wie kann Lützerath noch gerettet werden?
      </button>
    </h2>
    <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3" data-bs-parent="#accordionExample">
      <div class="accordion-body">
<p>Dörfer zu zerstören oder zu erhalten ist eine Frage des politischen Wollens. Galt früher der Abriss der Dörfer Holzweiler, Keyenberg, Berverath, Kuckum, Oberwestrich und Unterwestrich als ausgemachte Sache und alternativlose Notwendigkeit, ist heute klar, dass diese Dörfer durch den Druck von Umsiedlungsbetroffenen und Klimabewegten bleiben. Auch Lützerath kann erhalten werden, wenn es politisch gewollt ist.</p>

<p>In einer neuen Leitentscheidung kann die NRW-Landesregierung die räumlichen Grenzen des Tagebaus definieren. So hat sie <a href="https://www.bund-nrw.de/fileadmin/nrw/dokumente/braunkohle/2016_07_05_Leitentscheidung.pdf" target="_blank">2016</a> die Ortschaft Holzweiler am Tagebau Garzweiler und <a href="https://www.bezreg-koeln.nrw.de/brk_internet/gremien/braunkohlenausschuss/leitentscheidung/leitentscheidung_2021.pdf" target="_blank">2021</a> die Ortschaft Morschenich und den Hambacher Forst am Tagebau Hambach aus den Abbauplänen von RWE herausgenommen. Dies kann sie nun auch für Lützerath tun. Mit einem Abriss-Moratorium bis zur Veröffentlichung dieser Leitentscheidung kann sie dafür Sorge tragen, dass RWE keine Fakten schafft, die der neuen Leitentscheidung zuwider laufen.</p>

<p>Darüber hinaus läuft der Hauptbetriebsplan für den Tagebau Garzweiler am 31.12.2022 aus. RWE muss zur Fortführung des Kohleabbaus einen neuen Hauptbetriebsplan beantragen. Die Landesregierung könnte festlegen, dass Hauptbetriebspläne, die eine Zerstörung von Gebäuden, Infrastrukturen oder Bäumen beinhalten, nicht mehr genehmigt werden. Durch ein Abriss-Moratorium kann die Landesregierung Lützerath bis zum 01.01.2023 erhalten und durch angepasste Genehmigungsbedingungen für den Hauptbetriebsplan Lützerath dauerhaft bewahren.</p>
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="heading4">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
        Wer steht hinter X-Tausend für Lützerath?
      </button>
    </h2>
    <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="heading4" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>Die Kampagne X-Tausend für Lützerath wurde von Alle Dörfer Bleiben, LützerathLebt und FridaysForFuture initiiert. Über fünfzig Personen aus verschiedenen Organisationen und Gruppen waren an ihrer Entwicklung beteiligt. Eine Liste aller Unterstützer*innen gibt es <a href="/">hier</a>.</p>
      </div>
    </div>
  </div>


  <div class="accordion-item">
    <h2 class="accordion-header" id="heading5">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
        Was ist der Unterschied zwischen einer Petition und dieser Absichtserklärung?
      </button>
    </h2>
    <div id="collapse5" class="accordion-collapse collapse" aria-labelledby="heading5" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>Eine Petition richtet sich an politische Entscheidungsträger*innen. Sie können wichtige Werkzeuge in politischen Veränderungsprozessen sein und anzeigen, wie viele Menschen hinter einer bestimmten Forderung stehen. Mit <a href="https://weact.campact.de/petitions/kein-weiteres-dorf-mehr-fur-kohle-fur-klimagerechtigkeit-hier-und-uberall" target="_blank">dieser Petition</a> sprechen sich beispielsweise über Hunderttausend Personen für den Erhalt aller vom Kohleabbau bedrohten Dörfer in Deutschland aus. </p>

<p>Mit dieser Absichtserklärung bitten wir nicht andere um etwas, sondern kündigen an, selbst etwas zu tun. Damit senden wir ein ermutigendes Signal an alle Klimabewegten, die sehen: Wir sind X-Tausende, die im Herbst Lützerath erhalten werden! Und der Politik machen wir klar, dass sie mit massiven Protesten rechnen muss, sollte sie Lützerath räumen und abreißen wollen. </p>
      </div>
    </div>
  </div>
 <div class="accordion-item">
    <h2 class="accordion-header" id="heading6">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
        Warum läuft die Absichtserklärung über WeAct und was ist WeAct?
      </button>
    </h2>
    <div id="collapse6" class="accordion-collapse collapse" aria-labelledby="heading6" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>Damit technisch und datenschutzrechtlich größtmögliche Sicherheit besteht, unterstützt uns <a href="https://weact.campact.de/petitions/kein-weiteres-dorf-mehr-fur-kohle-fur-klimagerechtigkeit-hier-und-uberall" target="_blank">WeAct</a> bei der Umsetzung unserer Kampagne, indem WeAct das Formular zum Unterzeichnen der Absichtserklärung bereitstellt und die angegebenen Daten DSGVO-konform verwaltet. WeAct ist die Petitionsplattform von Campact – einer Bürger*innenbewegung, die sich mit über 2,3 Millionen Menschen für progressive Politik einsetzt. Über WeAct haben sich bei einer <a href="https://weact.campact.de/petitions/kein-weiteres-dorf-mehr-fur-kohle-fur-klimagerechtigkeit-hier-und-uberall" target="_blank">Petition</a> von AlleDörferBleiben bereits über 100.000 Menschen für den Erhalt aller vom Kohleabbau bedrohten Dörfer in Deutschland ausgesprochen. </p>
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="heading7">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
        Wie kann ich mitmachen?
      </button>
    </h2>
    <div id="collapse7" class="accordion-collapse collapse" aria-labelledby="heading7" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>Unsere Bewegung lebt davon, dass viele Menschen mitmachen. Dafür gibt es ganz unterschiedliche Wege: Du kannst in einer Klimagruppe in deiner Stadt einsteigen, nach Lützerath fahren und vor Ort aktiv werden, weitere Unterzeichner*innen für die Absichtserklärung mobilisieren und vieles mehr. Um dich bei X-Tausend für Lützerath zu engagieren, schreibe uns bitte eine E-Mail an <a href="mailto:kontakt@x-tausend.de">kontakt@X-Tausend.de</a></p>
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="heading8">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
        Wie kann meine Gruppe oder Organisation als Unterstützerin gelistet werden?
      </button>
    </h2>
    <div id="collapse8" class="accordion-collapse collapse" aria-labelledby="heading8" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>Wir freuen uns sehr, wenn weitere Gruppen und Organisationen die Kampagne X-Tausend für Lützerath unterstützen! Schicke bitte euer Logo in ausreichend guter Bildqualität an <a href="mailto:kontakt@x-tausend.de">kontakt@X-Tausend.de</a></p>
      </div>
    </div>
  </div>

</div>





