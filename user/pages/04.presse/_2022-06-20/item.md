---
title: "20.06.2022: Tausende kündigen an, Räumung von Braunkohle-Dorf zu verhindern"
---

Lützerath. In einer öffentlichen Absichtserklärung haben über 5000 Menschen angekündigt, sich der Zerstörung Lützeraths in den Weg zu stellen, falls die Landesregierung von NRW das Dorf räumen lassen will. Mit dabei sind Anwohnende, Aktive von Fridays For Future, ehemalige Mitglieder des Bundestages sowie die Geschäftsführer von Umweltverbänden. Lützerath liegt an der Kante des Braunkohle-Tagebaus Garzweiler II und ist akut von der Abbaggerung durch den Kohlekonzern RWE bedroht. Die Absichtserklärung findet sich unter [www.x-tausend.de](https://www.x-tausend.de) und kann weiterhin unterzeichnet werden.


Die Erklärung verweist darauf, dass eine Verbrennung der Kohleschicht unter Lützerath mit der Einhaltung der 1,5 Grad-Grenze und Klimagerechtigkeit unvereinbar sei. Mit der Absichtserklärung stellen sich die Unterzeichner*innen an die Seite von Gemeinschaften im globalen Süden, für die schon die jetzige Klimaerhitzung „die Hölle“ sei.




###### Alexandra Brüne, Alle Dörfer Bleiben: 
"Jahrzehnte lang haben unsere Mahnwachen, Demos und kulturellen Veranstaltungen nicht dazu geführt, dass Politiker*innen Klimaschutz endlich ernst nehmen und die Kohle im Boden lassen. Hilflos musste ich mitansehen, wie die Dörfer in meiner Nachbarschaft zerstört wurden. Jetzt möchte ich mit vielen Menschen Lützerath vor dieser Zerstörung bewahren und meinen Beitrag dazu leisten, die 1,5 Grad-Grenze einzuhalten."


###### Darya Sotoodeh, Fridays for Future:
"In Lützerath sehen wir leider weiterhin, wie die Politik und RWE die Klimakrise eskalieren lassen und unsere Lebensgrundlagen zerstören. Es ist ein Armutszeugnis für die Landesregierung, dass es wieder Aufgabe der jungen Menschen ist, sich dieser Zerstörung entgegen zu stellen. Im Angesicht der eskalierenden Klimakrise ist es aber so wichtig, dass die Politik endlich im Namen der Wissenschaft handelt. Und das heißt: Lützerath bleibt."


###### Dirk Jansen, Bund für Umwelt und Naturschutz Deutschland: 
"Wir anerkennen die geplante weitere Verkleinerung des Braunkohlentagebaus Garzweiler, das reicht aber nicht aus. Es gibt keine energiewirtschaftliche Begründung zur Zerstörung Lützeraths für die Kohlegewinnung. Auch der schreckliche Ukraine-Krieg darf dafür nicht als Argument missbraucht werden. Deshalb müssen die Braunkohlebagger vor Lützerath gestoppt werden. Die Braunkohle-Region muss endlich endgültig befriedet werden."


###### Lakshmi Thevasagayam, Lützerath Lebt: 
"Wir leben in einer Zeit in der uns mehrere Krisen überrollen - dabei merken wir immer mehr, dass das kapitalistische Wirtschaftssystem diese Krisen ausnutzt und antreibt um Profit zu machen. Die Politik stoppt das nicht - im Gegenteil: Die letzte Bundesregierung sicherte den Kohleabbau für den Tagebaubetreiber durch ein Gesetz, welches auf von RWE bezahlten Gutachten fußt. Wenn Schwarz-Grün in NRW nun weiter die Augen vor der Klimakatastrophe verschließen und dreckige Kohle abbaggern will, müssen wir uns organisieren um ein lebenswertes Leben für alle zu ermöglichen. In Indien fallen Menschen um vor der Hitze, Vögel fallen vom Himmel, in Italien fallen Ernten für das gesamte Jahr aus - was brauchen wir noch, um zu handeln?"


###### Christoph Bautz, Geschäftsführer von Campact:
"Derzeit erleben wir eine weitere Dürre in Deutschland, in Brandenburg mussten wegen riesigen Waldbränden sogar Orte evakuiert werden – im Juni! Wir sind mitten in der Klimakatastrophe, wir können jetzt nicht weiter riesige Kohle-Vorkommen erschließen. Die Bundesregierung muss ihr 1,5°-Versprechen umsetzen, jetzt den Kohleausstieg umsetzen und Lützerath erhalten. Wenn sie es nicht tut, werde ich an der Seite von X-Tausenden sein, die eine Räumung des Klimadorfs verhindern."


###### Dina Hamid, Ende Gelände: 
"Jedes Kind weiß heute, dass das dreckige Geschäft mit der Braunkohle in Deutschland ein für allemal vorbei sein muss. Auf einem Planeten, wo sich Extremwetter und Klima Katastrophen immer mehr häufen, wissen wir, dass auch die 650 Millionen Tonnen Kohle unter Lützerath einen Unterschied machen können, wenn sie im Boden bleiben. Die Landesregierung setzt das falsche Signal, wenn sie gegenüber dem Energiekonzern RWE klein beigibt. Die Todesopfer, die die Klimakrise schon heute fordert, scheinen nicht zu interessieren. Aber sie interessieren uns und wir machen den Unterschied: zusammen können wir Lützerath verteidigen. X-tausend Menschen sind bereit, sich mit ihren Körpern in den Weg der Zerstörung zu stellen."



###### Ein Statement der Autor*innen der DIW Studie "Kein Grad weiter – Anpassung der Tagebauplanung im Rheinischen Braunkohlerevier zur Einhaltung der 1,5-Grad-Grenze", Prof. Dr. Pao-Yu Oei und Catharina Rieve, DIW Berlin:
"Es besteht derzeit keine energiewirtschaftliche Notwendigkeit für einen kompletten Aufschluss des Tagebaus Garzweiler II, welcher die Umsiedlung weiterer Dörfer inklusive Lützerath bedingen würde. Die schnellstmögliche Reduktion und der vollständige Ausstieg aus der Kohlenutzung bleibt weiterhin eine Bedingung für die Einhaltung der (inter-)nationalen Klimaschutzziele. Die Energiewende muss jetzt entschlossener denn je forciert werden." 



### Kontakte
www.x-tausend.de
 
Alexandra Brüne, Alle Dörfer Bleiben: +49 173 5176392, presse@alle-doerfer-bleiben.de

Fridays For Future: +49 151 40101583, presseteam@fridaysforfuture.de

Dina Hamid, Ende Gelände: +49 152 12999905

Dirk Jansen, BUND NRW, +49 172 29 29 733, dirk.jansen@bund.net

Lakshmi Thevasagayam, Lützerath Lebt!: +49 1575 3980277, kontakt@luetzerathlebt.info
