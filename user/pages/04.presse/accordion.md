---
title: Presse

content:
    items: '@self.modular'
--- 
<br>

#### Pressekontakt

Vielen Dank für Ihr Interesse an “X-Tausend für Lützerath“! Bei Fragen kontaktieren Sie gerne:

- Alle Dörfer Bleiben: +49 1577 3395845, [presse@alle-doerfer-bleiben.de](mailto:presse@alle-doerfer-bleiben.de)
- Fridays For Future: +49 151 40101583, [presseteam@fridaysforfuture.de](mailto:presseteam@fridaysforfuture.de)
- Lützerath Lebt!: +49 1575 3980277, [kontakt@luetzerathlebt.info](mailto:kontakt@luetzerathlebt.info)

#### Pressemitteilungen