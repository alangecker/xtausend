---
title: "23.06.2022: Braunkohle: NRW-Koalitionsvertrag macht Moratorium für Lützerath notwendig + Über 7000 Menschen kündigen an, das Dorf zu schützen"
---

Erkelenz/Düsseldorf, 23. Juni 2022. Alle Dörfer bleiben, Fridays For Future und Lützerath Lebt stellen klare Forderungen an die Braunkohlepolitik der neuen Landesregierung NRW, wie sie im heute vorgestellten Koalitionsvertrag vereinbart wurde. Der Vertrag sieht zwar einen Kohleausstieg 2030 und den Erhalt von fünf Dörfern am Tagebau Garzweiler II vor, macht aber keine explizite Aussage zum Erhalt des umkämpften Dorfes Lützerath. Jedoch kündigt die Landesregierung Gespräche mit RWE an, um festzulegen, welche Flächen bis zur nächsten Leitentscheidung verschont werden können. Die Bündnisse fordern die Landesregierung dazu auf, über diese Gespräche den Erhalt von Lützerath zu erwirken. Zudem müsse die Ausweitung des Tagebaus sofort gestoppt und die Menge an Kohle, die noch gefördert werden darf, auf ein festes Budget begrenzt werden. Nur so sei es möglich, die 1,5 Grad-Grenze noch einzuhalten. Sollte es doch noch zu einer Räumung des Dorfes kommen, kündigen die Bündnisse Widerstand an: Seit letztem Freitag haben über 7000 Menschen eine Erklärung unterzeichnet, dass sie persönlich nach Lützerath kommen werden, um den Ort im Falle einer Räumung zu schützen.

Alexandra Brüne von Alle Dörfer Bleiben kommentiert: „In Indien sind Millionen von Menschen obdachlos geworden, weil der Monsun verrückt spielt. Auch hierzulande brennen die Wälder, Getreide ist auf dem Acker verdorrt. Daher ist es das Mindeste, dass die neue Landesregierung Gespräche über den Erhalt von Lützerath mit RWE führen wird. Aus Sicht der Klimawissenschaft gibt es aber nur ein Ergebnis: Lützerath muss erhalten bleiben, damit Deutschland 1,5° einhalten kann.”

Unter www.x-tausend.de haben über 7000 Menschen angekündigt, dass sie persönlich nach Lützerath kommen werden, um den Ort zu schützen. „Wenn die Landesregierung Lützerath räumen und abreißen will, werden wir vor Ort sein und uns der Zerstörung in den Weg stellen“ heißt es in der Absichtserklärung. Unter den Unterzeicher*innen finden sich neben bekannten Menschen der Klimabewegung wie Luisa Neubauer (Fridays for Future) und Dina Hamid (Ende Gelände) auch die Grünen-Bundestagsabgeordnete Kathrin Henneberger sowie die Geschäftsführer des BUND NRW und der NGO Campact.

Liva Rudroff von Fridays For Future dazu: „Die Zerstörung von Lützerath würde das Reißen der kritischen 1,5 Grad-Grenze bedeuten. Den Preis dafür zahlen Menschen weltweit jetzt schon. Wir können in Anbetracht der eskalierenden Klimakrise nicht einfach zusehen, wie notwendige politische Maßnahmen wiederholt ausbleiben. Schon jetzt haben Tausende Menschen angekündigt, sich im Falle einer Räumung der Zerstörung von Lützerath in den Weg zu stellen. Die Landesregierung muss nun alles daran setzen, in den angekündigten Gesprächen den Erhalt von Lützerath zu beschließen oder muss mit dem Widerstand der gesamten Klimabewegung rechnen.“

Lakshmi Thevasagayam von Lützerath Lebt erklärt: „Der IPCC fordert seit Jahren eine radikale Transformation unserer Energieversorgung. Im Koalitionsvertrag gibt es aber noch nicht einmal einen Plan zur Einhaltung des Pariser Klimaabkommens. Jede Tonne Kohle ist mittlerweile zu viel. Solange sich die Koalition noch nicht einmal an dieses Abkommen hält, verteidigen wir Lützerath.“

David Dresen von Alle Dörfer bleiben übt scharfe Kritik an den Plänen der Bundesregierung, die Reservekapazitäten von Kohlekraftwerken zu reaktivieren: „Dass Kohlekraft irgendetwas mit Versorgungssicherheit zu tun haben soll, ist angesichts der aktuell sichtbaren Klimafolge einfach der blanke Hohn. Fossile Energien bringen uns nicht raus aus der Krise, sondern tiefer herein. Eine vorwärtsgewande Politik würde nun daran gehen, Verschwendung und sinnlose Produktion zu reduzieren, um unseren Energieverbrauch drastisch zu senken. Wenn wir klug wirtschaften und die vorhandenen Ressourcen gerecht verteilen, muss niemand Mangel leiden.“

### Kontakt

David Dresen, Alle Dörfer Bleiben: +49 178 2334959, presse@alle-doerfer-bleiben.de

Lakshmi Thevasagayam, Lützerath Lebt: +49 1575 3980277

Liva Rudroff, Fridays For Future: +49 1575 2373307, nrw@fridaysforfuture.de