---

logos:
    - logo: adb_logo.png
      url: "https://www.alle-doerfer-bleiben.de/"
    - logo: FFF_logo.png
      url: "https://fridaysforfuture.de/"  
    - logo: luetzerath_lebt_logo.png
      url: "https://luetzerathlebt.info/"  
---
