---
title: 'Text & Formular'
petition: 'https://weact.campact.de/petitions/absichtserklarung-klima-schutzen-lutzerath-retten'
signature_text: 'Wenn die Landesregierung Lützerath räumen und abreißen will, werde ich vor Ort sein und mich der Zerstörung in den Weg stellen.'
signature_count_text: 'Schon COUNT werden kommen, du auch?'
promis:
    -
        name: 'Dirk Jansen'
        function: 'Geschäftsführer BUND NRW'
    -
        name: 'Kathrin Henneberger'
        function: 'MdB, Bündnis 90/Die Grünen'
    -
        name: 'Luisa Neubauer'
        function: 'Fridays for Future'
    -
        name: 'Dina Hamid'
        function: 'Ende Gelände'
    -
        name: 'Christoph Bautz'
        function: 'Geschäftsführer Campact'
    -
        name: 'Lakshmi Thevasagayam'
        function: 'Lützerath Lebt'
    -
        name: 'Carola Rackete'
        function: 'Ökologin und Antifaschistin'
    -
        name: 'Lorenz Gösta Beutin'
        function: 'Klimapolitiker im Parteivorstand DIE LINKE'
---

> _„Jetzt ist es an der Zeit, die Wut in Taten umzusetzen. Jeder Bruchteil eines Grades zählt.“_
> <small>UN-Generalsekretär Guterres bei der Vorstellung des Weltklima-Berichts 2022</small>

Ab Januar wird es ernst: Der Kohlekonzern RWE, die Landesregierung NRW und die Bundesregierung wollen das Dorf Lützerath räumen und abreißen. Unter dem Dorf liegen riesige Mengen Braunkohle. Insgesamt will RWE aus dem Tagebau noch 280 Mio t Kohle fördern, das ist sechs Mal mehr als unsere letzte Chance auf 1,5° Klimerhitzung zulässt. Und das, wo schon jetzt Millionen Menschen im Globalen Süden durch die Klimakrise sterben oder ihre Zuhause verlieren. Mit dieser Erklärung verkünden wir, die Unterzeichnenden, unsere Absicht vor Ort zu sein und uns der Zerstörung in den Weg zu stellen, sollte die Landesregierung versuchen, Lützerath zu räumen.

<hr class="mb-5 mt-5">

##### **Update Dezember 2022:** 
**Wir rechnen mit einem Räumungsversuch ab der zweiten Januar-Woche 2023. Vorher wird es vor Ort und in vielen Städten Aktionstrainings geben. Am 14.1. wird es eine große Demo am Tagebau geben. Wenn Ihr Lützerath direkt schützen wollt, ist es gut, bis zum 8.1. anzureisen.** Vor Ort hat sich ein vielfältiger Widerstand entwickelt: es gibt ein Protestcamp, zahlreiche besetzte Häuser, wöchentliche Dorfspaziergänge und vieles mehr. 

<br/>


## **Lützi bleibt! Unserem Aufruf folgen bereits über 10.000 Menschen. [Du auch?](https://www.x-tausend-luetzerath.de/#unterzeichnen)**

**Die Gründe hierfür sind klarer denn je:** RWE prognostiziert Rekord-Gewinne, während die Strompreise weiter in die Höhe schnellen und noch mehr Menschen in Armut geraten. Anstatt alle sicher zu versorgen, versorgt die Politik die Profite und den Strombedarf von Konzernen und Industrie, den Hauptverbrauchern des Kohlestroms. Diesem Profit wird der Klimaschutz geopfert. RWE darf sechs mal mehr Kohle verbrennen, als es das Restbudget im Rahmen des Pariser Klimaschutzabkommens erlaubt. Die 1,5 Grad-Grenze verläuft somit nicht nur symbolisch sondern ganz real vor Lützerath. Jede weitere Tonne Kohle verwüstet nicht nur den Boden und die Natur, sondern erzeugt überall, vor allem in Ländern des Globalen Südens, noch mehr Leid und Tote.

Der Deal, den uns die Landesregierung als Erfolg für den Klimaschutz verkauft, ändert daran nichts: Wissenschaftler:innen des Deutschen Instituts für Wirtschaftsforschung (DIW) haben aufgedeckt, dass der vorgezogene Kohleausstieg nur wenige oder sogar gar keine CO2-Einsparungen ermöglicht - denn RWE darf nun einfach schneller Kohle abbauen und verfeuern! Und dafür will der Kohlekonzern den Nachschub unter Lützerath zeitnah. **Wir stellen uns hier in den Weg, damit die Kohle unter Lützerath bleibt!**

## **[Unterzeichne jetzt](https://www.x-tausend-luetzerath.de/#unterzeichnen) diesen Aufruf, falls du es noch nicht getan hast.**

<br/>
 
 
  
<div style="text-align:center">
<a href="https://www.youtube.com/watch?v=TcHBtk_tsiM" target="_blank"><img src="/user/pages/01.home/video-placeholder.jpg" alt="Neues Mobi-Video (Link zu youtube.com)"></a>
</div>

<hr class="mb-5 mt-5">

##### **Weiter mit dem initialen Aufruf vom Sommer 2022:** 
Die Fakten sind klar. **Wenn nicht jetzt sofort umgelenkt wird, sind alle Bemühungen, die 1,5°C-Grenze noch einzuhalten und damit die Auswirkungen der Klimakatastrophe einzudämmen, zum Scheitern verurteilt.** Seit Jahrzehnten fordern Wissenschaftler\*innen, Betroffene und Aktivist\*innen, unverzüglich Kohle, Öl und Gas im Boden zu lassen. Wir müssen unser Wirtschaftssystem umbauen, um das Überleben von Millionen Menschen und eine gerechte Gesellschaft zu ermöglichen.

Die Klimaerhitzung führt zu immer mehr und heftigeren Extremwettern – derzeit unter anderem eine dramatische Hitzewelle in Indien, eine verheerende Dürre im Osten Kenias und über 40 Grad erhöhte Durchschnittstemperaturen in der Antarktis. Aufgrund der rasanten Klimaerhitzung befinden wir uns in einem sechsten Massenaussterben von Tier- und Pflanzenarten. **Die Klimaaktivistin Vanessa Nakate aus Uganda erinnert daran, dass die aktuelle Klimaerhitzung um 1,2 Grad "die Hölle“ für viele Menschen und Gemeinschaften im Globalen Süden ist.** Diese Gemeinschaften haben am wenigsten zur Klimakrise beigetragen und kämpfen zugleich schon am längsten gegen die globale Ungerechtigkeit, die sie hervorgebracht hat. Wir stellen uns an ihre Seite.

Deutschlands Verantwortung in Lützerath ist offensichtlich. Das Rheinische Kohlerevier ist Europas größte CO2-Schleuder. RWE plant, zwischen Aachen, Köln und Düsseldorf mehrere hundert Millionen Tonnen abzubauen und zu verbrennen. Mit dem 1,5°C-Limit sind diese Mengen nicht vereinbar. **Unter Lützerath ist die Kohleschicht besonders groß, weshalb der Erhalt des Dorfes zu einer besonders großen Einsparung von CO2 führt.** Keine andere Maßnahme kann so schnell und so einfach CO2 einsparen.

Diese Chance müssen wir nutzen. **Die Regierung in Nordrhein-Westfalen hätte die Möglichkeit, den Erkenntnissen der Wissenschaft, den Forderungen von Klimawandel-Betroffenen und den Bedürfnissen zukünftiger Generationen zu entsprechen** und ihren Worten endlich Taten folgen zu lassen. Stattdessen versuchen CDU und GRÜNE, Lützerath als nettes, aber letztlich belangloses 'Symbol' kleinzureden, um ihren Koalitionsfrieden aufzubauen und den Kohlekonzern RWE nicht zu verärgern. **Ihr Kalkül ist, dass wir schon nicht so viele und nicht so entschlossen sein werden.** Sie spekulieren darauf, dass eine Räumung Lützeraths rasch vorbei ist und schnell in Vergessenheit gerät.

**Damit begehen sie einen großen Fehler.** Seit vielen Jahren protestieren Anwohnende und Umsiedlungsbetroffene, Klimaaktive, Landwirt\*innen, Großeltern, Geistliche, Kulturschaffende und Prominente mit vielen Tausend Menschen dafür, dass alle Dörfer bleiben. **Durch dieses Engagement konnten bereits einige Dörfer im Rheinland erhalten bleiben – und wir sind fest entschlossen, auch Lützerath zu retten.**

Vor vier Jahren haben zehntausende Menschen in Baumhäusern, vor Gericht und auf der Straße den Erhalt des Hambacher Waldes erstritten. **Gemeinsam haben wir dafür gesorgt, dass 1,1 Milliarden Tonnen Kohle im Boden bleiben.** Noch während der dreiwöchigen Räumung – dem widerrechtlichen und größten Polizeieinsatz in der Geschichte Nordrhein-Westfalens – behauptete der RWE-Chef, dass der Erhalt des Waldes eine ‚Illusion‘ sei. Unsere massenhaften Proteste, die solidarische Vielfalt unserer Aktionsformen und unsere Entschlossenheit haben diese ‚Illusion‘ Wirklichkeit werden lassen. **Wir haben klargemacht, dass nicht der Erhalt unserer Lebensgrundlagen, sondern die fossile, profitorientierte Gesellschaftsordnung die eigentliche Illusion ist.** Diese Erfahrung nehmen wir mit nach Lützerath und teilen sie mit allen, die sich für Klimagerechtigkeit einsetzen: Eine andere Welt ist möglich!

Die Zeit des Wartens ist vorbei. Die Zeit der leeren Worte ist vorbei. **Die Zeit des Handelns ist gekommen.**
Mit dieser Erklärung schließen wir uns zusammen, um mit X-Tausenden Menschen Lützerath zu erhalten und das Klima zu schützen. Sollte die Landesregierung in NRW es wagen, Lützerath für den Kohleabbau zerstören zu wollen, werden wir das nicht hinnehmen. Die Kohle unter Lützerath muss im Boden bleiben. 

## **[Unterzeichne jetzt!](https://www.x-tausend-luetzerath.de/#unterzeichnen)**
