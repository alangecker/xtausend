---
title: Unterstützer:innen

people:
    - name: XR Deutschland 
      url: https://extinctionrebellion.de/
    - name: XR Bielefeld
      url: https://extinctionrebellion.de/og/bielefeld/
    - name: Parents4Future Köln
      url: https://koelle4future.de/
    - name: Omas4Future Germany
      url: https://www.omasforfuture.de/
    - name: RWE Tribunal
      url: https://www.rwe-tribunal.org/
    - name: Mahnwache Lützerath
      url: https://mahnwache-luetzerath.org/
    - name: ausgeCO2hlt 
      url: https://ausgeco2hlt.de/
    - name: 1,5-Grad-Mahnwache Essen
    - name: Parents4Future München
      url: https://parentsforfuture.de/de/muenchen
    
    - name: BI Lüchow Dannenberg
      url: https://www.bi-luechow-dannenberg.de/
    - name: Waldspaziergang
    - name: ethecon- Stiftung Ethik & Ökonomie
      url: https://www.ethecon.org/
    - name: Die Kirche(n) im Dorf lassen
      url: https://www.kirchen-im-dorf-lassen.de/
    - name: Parents for Future Herford
      url: https://parentsforfuture.de/de/node/2001

    - name: Parents For Future Leipzig
      url: https://parentsforfuture.de/de/leipzig

    - name: Hambi Support München
      url: https://twitter.com/hambisupportmuc

    - name: XR Essen
      url: https://extinctionrebellion.de/og/essen/

    - name: XR Bonn
      url: https://extinctionrebellion.de/og/bonn/

    - name: Hambi Support Aachen
    
    - name: Antikapitalistisches Klimatreffen Karlsruhe
      url: https://www.klimabuendnis-karlsruhe.de
    
    - name:  Initiative Gewerkschafter*innen für den Klimaschutz
      url: https://www.labournet.de/branchen/energie/klima/gewerkschafterinnen-fuer-klimaschutz/?cat=8320

    - name: Ende Gelände goes Lützerath
      url: https://www.ende-gelaende.org/ende-gelaende-goes-luetzerath/

    
    - name: Klimabaumhaus Bayreuth
      url: https://www.klimacamp-bayreuth.de/
    
    - name: Ende Gelände Hamburg
    
    - name: Campus:grün Köln
      url: http://www.campusgruen.uni-koeln.de
      
    - name: Antikapitalistisches Klimatreffen Tübingen
    
    - name: Parents for Future Germany
      url: https://parentsforfuture.de

    - name: Ende Gelände
      url: https://www.ende-gelaende.org

# - name: Einzelperson
    #   pic:
    #   position: Fridays for Future
    
    # - name: Einzelperson
    #   pic:
    #   position: Fridays for Future
      
    # - name: Einzelperson
    #   pic:
    #   position: Fridays for Future
            

description: Wenn die Landesregierung Lützerath räumen und abreißen will, werden wir vor Ort sein und uns der Zerstörung in den Weg stellen.       
---

<h2> Unterstützer*innen </h2>
