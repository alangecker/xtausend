---
title: 'Text & Formular'
classes: 'mt-2 pt-5'
petition: 'https://weact.campact.de/petitions/absichtserklarung-klima-schutzen-lutzerath-retten'
show_english_note: 'yes'
signature_text: 'If the state government wants to clear and demolish Lützerath, I will be there to stand in the way of the destruction.'
signature_count_text: 'COUNT people will come and protect Lützerath. You too?'
---

> _„Now is the time to turn rage into action. Every fraction of a degree matters.“_
> <small>António Guterres, United Nations Secretary-General, at the presention of the climate report of the UN 2022</small>


Starting in January, things will get serious: The coal corporation RWE, the state government of North Rhine-Westphalia and the federal government of Germany want to evict and demolish the village of Lützerath. Huge quantities of coal beneath the village. RWE wants to extract a total of 280 million tons of coal from the open pit mine, six times more than our last chance of 1.5° climate warming. And this, while millions of people in the Global South are already dying or losing their homes due to the climate crisis. With this statement, we, the signees, announce our intention to be in Lützerath and stand in the way of destruction, should the government try to evict and demolish Lützerath.

<hr class="mb-5 mt-5">

##### **Update December 2022:** 
**We expect an eviction attempt starting in the second week of January 2023. Before that, there will be action trainings on site and in many cities. On Jan. 14, there will be a big demonstration at the open pit mine. If you want to protect Lützerath directly, it is good to arrive by Jan. 8.** In the village, a diverse resistance has grown: there's a protest camp, numerous occupied houses, weekly village walks and much more. 

<br/>
## **Lützi stays! More than 11,000 people are already following our call. [You too?](https://www.x-tausend-luetzerath.de/#unterzeichnen)**

**The reasons are clearer than ever:** RWE is forecasting record profits while electricity prices have skyrocketed and even more people are falling into poverty. Instead of safely supplying everyone, politicians are supplying the profits and electricity needs of corporations and industry, the main consumers of coal-fired electricity. Climate protection is sacrificed for this profit. RWE is allowed to burn six times more coal than the remaining budget under the Paris Climate Agreement allows. Therefore, the 1.5 degree limit lies in front of Lützerath not only symbolically, but in very real terms. Every additional ton of coal not only devastates soil and nature, but creates even more suffering and death everywhere, especially in countries of the Global South.

The deal that the state government is selling us as a success for climate protection does nothing to change this: Scientists at the German Institute for Economic Research (DIW) have uncovered that the early coal phase-out will enable only a few or even no CO2 savings – because RWE is now simply allowed to mine and burn coal faster! And for it the coal company wants the supply under Lützerath as quickly as possible. **We will stand in their way, so that the coal stays under Lützerath!

## **[Sign now](https://www.x-tausend-luetzerath.de/#unterzeichnen) if you haven't already.** ##
<br/>

<div style="text-align:center">
<a href="https://www.youtube.com/watch?v=TcHBtk_tsiM" target="_blank"><img src="/user/pages/01.home/video-placeholder.jpg" alt="Neues Mobi-Video (Link zu youtube.com)"></a>
</div>

<hr class="mb-5 mt-5">

##### **Continue with initial call from Summer 2022:** 

The facts are clear. If we don‘t change direction now, all efforts to meet the 1.5°C limit and thus contain the effects of the climate catastrophe are doomed to failure. For decades, scientists, affected communities and activists have demanded to leave coal, oil and gas in the ground without delay. We must transform our economic system to enable the survival of millions of people and a just society.

Global heating is leading to more and more severe extreme weather - currently including a dramatic heat wave in India, a devastating drought in eastern Kenya, and over 40 degrees increased average temperatures in Antarctica. Due to rapid global heating, we are in the midst of a sixth mass extinction of animal and plant species. Climate activist Vanessa Nakate from Uganda reminds us that the current 1.2 degrees of global heating is "hell" for many people and communities in the Global South. These communities have contributed the least to the climate crisis, and at the same time have been fighting the global injustice that has created it the longest. We stand with them.

Germany's responsibility in Lützerath is obvious. The coalfields in the Rhineland are Europe's biggest source of CO2. RWE plans to mine and burn several hundred million tons between Aachen, Cologne and Düsseldorf. These quantities are not compatible with the 1.5°C limit. Under Lützerath the coal layer is particularly thick, which is why the preservation of the village leads to a particularly large saving of CO2. No other measure can save CO2 as quickly and as easily.

We must seize this opportunity. The government in North Rhine-Westphalia has the opportunity to respond to the findings of science, the demands of those affected by climate change and the needs of future generations – and finally follow its words up with action. Instead, the conservative CDU and the Green Party are trying to play down Lützerath as a nice but ultimately irrelevant 'symbol' in order to build their coalition and not to upset the coal company RWE. Their calculation is that in the end we will not be that many and not that determined. They‘re speculating that an eviction of Lützerath will be over and forgotten quickly.

But they‘re making a big mistake. For many years, residents subject to forced resettlements, climate activists, farmers, grandparents, people of faith, artists and celebrities have protested with many thousands of people for all villages to remain. Thanks to this commitment, several villages in the Rhineland have already been saved - and we are determined to save Lützerath as well.

Four years ago, tens of thousands of people in tree houses, in courts and on the streets fought to preserve the Hambach Forest. Together, we ensured that 1.1 billion tons of coal remained in the ground. Even during the three-week eviction - the illegal and largest police operation in the history of North Rhine-Westphalia - the RWE boss claimed that preserving the forest was an 'illusion'. Our mass protests, the solidarity-based diversity of our actions and our determination turned this 'illusion' into reality. We have made clear that the real illusion is not the preservation of our livelihoods, but the fossil-fuel, profit-oriented social order. We take this experience with us to Lützerath and share it with all those who are committed to climate justice: Another world is possible!

The time of waiting is over. The time of empty words is over. The time for action has come. With this declaration, we join together with X-thousands of people to preserve Lützerath and protect the climate. If the state government in North-Rhine Westfalia wants to destroy Lützerath for coal mining, we will not accept it. The coal under Lützerath must stay in the ground. 