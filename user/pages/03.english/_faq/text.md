---
title: FAQ
---

## FAQ


<br>

<div class="accordion mx-2" id="accordionExample">

  <div class="accordion-item">
    <h2 class="accordion-header" id="heading1">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
        What is Lützerath?
      </button>
    </h2>
    <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1" data-bs-parent="#accordionExample">
      <div class="accordion-body">
<p>Lützerath is the last village to be mined for coal in the Rhenish coalfield in Germany. The edge of the Garzweiler open cast lignite mine is less than 200 meters from houses of Lützerath. The coal corporation RWE plans to clear and demolish Lützerath this autumn/winter. Preserving Lützerath and leaving the coal in the ground beneath the village is one of the most effective climate protection measures Germany can currently take.</p>

<p>In recent years, Lützerath has become a focal point for the climate justice movement. Hundreds of climate activists have revived Lützerath, built tree houses and wooden huts there, and made the village their home. Again and again, several thousand people have gathered in the village for demonstrations, festivals and conferences. Actually, RWE wanted to destroy it long ago, but social pressure and legal proceedings have prevented this so far - now we want to save the village permanently.</p>

<p>On 01 September 2022, the buildings and land of farmer Eckardt Heukamp will officially belong to RWE. It can be assumed that RWE wants to clear and demolish Lützerath this autumn. Similar to the struggle in the Hambach Forst, this can only be done with a politically risky large-scale police operation. X-thousands of people will protest on the streets, on trees and in local houses for the protection of the climate and the preservation of the village and will stand in the way of the destruction. </p>
      </div>
    </div>
  </div>


  <div class="accordion-item">
    <h2 class="accordion-header" id="heading2">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
        Why must Lützerath remain?
      </button>
    </h2>
    <div id="collapse2" class="accordion-collapse collapse" aria-labelledby="heading2" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>A particularly thick layer of lignite lies beneath Lützerath. Burning it massively harms our global climate.</p>
        <blockquote>
          <div class="row mb-3">
          <img>
            <div class="col-1 text-end"><img src="https://nitter.fdn.fr/pic/profile_images%2F1479884089582891010%2Fhv0SVCEl_bigger.jpg" style="max-width: 100%;max-height: 3em; border-radius:1.5em;border: 1px solid #ddd" /></div>
            <div class="col-9"><b>Alle Dörfer Bleiben</b><br><a href="https://twitter.com/AlleDoerfer" target="_blank">@AlleDoerfer</a></div>
            <div class="col-1 text-end fs-2"><a href="https://nitter.fdn.fr/AlleDoerfer/status/1535550955420372992" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
          </div>
          
          <p style="font-size:0.9em;font-family:sans-serif">Current planning status of Garzweiler open pit mine:<br>
          🟤= current excavator progress<br>
          ⚫️= saved by NRW government in 2016<br>
          🟢= saved by NRW exploratory paper<br>
          ⚠️ Everything else <a href="https://twitter.com/CDUNRW_de" target="_blank">@CDUNRW_de</a> & <a href="https://twitter.com/gruenenrw" target="_blank">@gruenenrw</a> still want to mine [The redder the areas, the more coal].</p>
          <img class="img-fluid" alt="map with current planning status" src="/user/pages/05.faq/karte.jpg" />
        </blockquote>
        <p>RWE plans to extract more than 600 million tons of coal from the Garzweiler open pit mine. Since one ton of lignite releases about one ton of CO2 when burned, the climate impact of these RWE plans is roughly equivalent to that of 150 million flights from Frankfurt to New York. These plans are not compatible with the Paris Climate Agreement. <a href="https://www.diw.de/documents/publikationen/73/diw_01.c.819609.de/diwkompakt_2021-169.pdf" target=_blank">A study</a> by the German Institute for Economic Research (DIW) shows that only 70 million metric tons of coal can be extracted from the Garzweiler open pit mine from January 1, 2021 in order to comply with the 1.5° limit. Although these reduced quantities also contribute to the worsening of the climate crisis, the study clearly shows: The red line for the climate runs in front of Lützerath! 
        </p>
      </div>
    </div>
  </div>

<div class="accordion-item">
    <h2 class="accordion-header" id="heading3">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
        How can Lützerath still be saved?
      </button>
    </h2>
    <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      <p>Destroying or preserving villages is a question of political will. If in former times the demolition of the villages Holzweiler, Keyenberg, Berverath, Kuckum, Oberwestrich and Unterwestrich was considered a foregone conclusion and a necessity without any alternative, today it is clear that these villages will remain due to the pressure of affected residents and climate activists. Lützerath can also be preserved if it is politically desired.</p>

<p>In a so-called „lead decision“ (Leitentscheidung), the NRW state government can define the spatial boundaries of the open pit mine. In <a href="https://www.bund-nrw.de/fileadmin/nrw/dokumente/braunkohle/2016_07_05_Leitentscheidung.pdf" target="_blank">2016</a>, for example, it removed the village of Holzweiler at the Garzweiler open pit mine from RWE's mining plans and, in <a href="https://www.bezreg-koeln.nrw.de/brk_internet/gremien/braunkohlenausschuss/leitentscheidung/leitentscheidung_2021.pdf" target="_blank">2021</a>, the village of Morschenich and the Hambach Forest at the Hambach open pit mine. The government can now do the same for Lützerath. By imposing a moratorium on demolition until the publication of this lead decision, it can ensure that RWE does not create any facts that run counter to the new policy.</p>

<p>In addition, the main operating plan for the Garzweiler open pit mine expires on December 31, 2022. RWE must apply for a new main operating plan to continue coal mining. The state government could stipulate that main operating plans involving the destruction of buildings, infrastructure or trees will no longer be approved. By imposing a moratorium on the demolition, the state government can preserve Lützerath until Jan. 1, 2023, and permanently preserve Lützerath by adjusting the conditions of approval for the main operating plan.</p>
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="heading4">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
        Who is behind „X-tausend für Lützerath“?
      </button>
    </h2>
    <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="heading4" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>The Campaign X-tausend für Lützerath was initiated by Alle Dörfer Bleiben, Lützerath Lebt and Fridays For Future. Over fifty people from different organizations and groups were involved in its development. A list of all supporters can be found <a href="/">here</a>.</p>
      </div>
    </div>
  </div>


  <div class="accordion-item">
    <h2 class="accordion-header" id="heading5">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
        What is the difference between a petition and a declaration of intent?
      </button>
    </h2>
    <div id="collapse5" class="accordion-collapse collapse" aria-labelledby="heading5" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>A petition is addressed to political decision-makers. Petitions can be important tools in political change processes and show how many people are behind a particular demand. With <a href="https://weact.campact.de/petitions/kein-weiteres-dorf-mehr-fur-kohle-fur-klimagerechtigkeit-hier-und-uberall" target="_blank">this petition</a>, for example, over a hundred thousand people are speaking out for the preservation of all villages in Germany that are threatened by coal mining.</p>

<p>With this declaration of intent however, we are not asking others to do something, but announcing that we will do something ourselves. In doing so, we are sending an encouraging signal to all climate activists who see: We are X-thousands who will preserve Lützerath! And we‘re making clear to politicians that they can expect massive protests if they want to evict and demolish Lützerath.</p>
      </div>
    </div>
  </div>
 <div class="accordion-item">
    <h2 class="accordion-header" id="heading6">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
        Why is the signing process run via WeAct and what is WeAct?
      </button>
    </h2>
    <div id="collapse6" class="accordion-collapse collapse" aria-labelledby="heading6" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <p>To ensure the greatest possible technical and data protection security, WeAct supports us in the implementation of our campaign by providing the form for signing the declaration of intent and managing the data provided in a GDPR-compliant manner. WeAct is the petition platform of Campact – a campaigning network and NGO with over 2.3 million people campaigning for progressive policies. Via WeAct, more than 100,000 people have already spoken out for the preservation of all villages in Germany threatened by coal mining in a petition by Alle Dörfer Bleiben.</p>
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="heading7">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
        How can I join?
      </button>
    </h2>
    <div id="collapse7" class="accordion-collapse collapse" aria-labelledby="heading7" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      <p>Our movement depends on many people joining in. There are many different ways to do this: You can join a climate group in your city, travel to Lützerath and become active on site, mobilize more signees for the declaration of intent, and much more. To get involved with X-Tausend für Lützerath, please send us an email to <a href="mailto:kontakt@x-tausend.de">kontakt@X-Tausend.de</a>.</p>
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="heading8">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
        How can an organisation or a group be listed as a supporter?
      </button>
    </h2>
    <div id="collapse8" class="accordion-collapse collapse" aria-labelledby="heading8" data-bs-parent="#accordionExample">
      <div class="accordion-body">
      <p>We are very happy if other groups and organizations support the campaign X-Tausend für Lützerath! Please send us an e-mail to <a href="mailto:kontakt@x-tausend.de">kontakt@X-Tausend.de</a>.</p>
      </div>
    </div>
  </div>

</div>





