---
title: Impressum
---
<br>

**Kib e.V.**<br>
c/o Kraneis<br>
Pödelwitz 2<br>
04539 Groitzsch<br>

Eingetragen im Amtsgericht Leipzig, VR 5497<br>
Vorstand: P. Aschenbrenner, U. Steil

E-Mail: kib@riseup.net

Titelfoto von Christoph Schnüll ([CC BY 2.0](https://creativecommons.org/licenses/by/2.0/))