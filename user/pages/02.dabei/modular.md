---
title: Schon Dabei
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
        custom:
            - _header

imports: 
    - 'user://data/signee.yaml'
    - 'user://data/promis.yaml'
---
