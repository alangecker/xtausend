---
title: Unterzeichnende
comment: Die Liste der Namen enthält nur diejenigen, die zugestimmt haben, dass ihr Name veröffentlich werden darf. Darüber hinaus wird die Liste manuell geupdated, weshalb es womöglich 1-2 Tage dauert bis dein Name erscheint.


---

> „Wenn die Landesregierung Lützerath räumen und abreißen will, werde ich vor Ort sein und mich der Zerstörung in den Weg stellen.“