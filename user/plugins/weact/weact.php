<?php
namespace Grav\Plugin;

use Composer\Autoload\ClassLoader;
use Grav\Common\Plugin;

/**
 * Class WeactPlugin
 * @package Grav\Plugin
 */
class WeactPlugin extends Plugin
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'onPluginsInitialized' => [
                // Uncomment following line when plugin requires Grav < 1.7
                // ['autoload', 100000],
                ['onPluginsInitialized', 0]
            ]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized(): void
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        // Enable the main events we are interested in
        $this->enable([
            'onTwigInitialized'   => ['onTwigInitialized', 0],
        ]);
    }

    public function onTwigInitialized()
    {
        $this->grav['twig']->twig()->addFunction(
            new \Twig_SimpleFunction('get_weact_petition', function($url) {
                $res = file_get_contents($url.".json");
                $data = json_decode($res);
                $data->signature_count_formatted = number_format($data->signature_count, 0, ',', '.');
                return $data;
                // TODO: caching
                // reference: https://gitlab.com/kanthaus/kanthaus.online/-/blob/master/user/plugins/remote-file/remote-file.php#L53=
            })
        );
    }
}
